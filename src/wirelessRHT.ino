/*************************************************************************************
    Includes
*************************************************************************************/
#include <time.h>
#include <LowPower.h>
#include "CRC.h"
#include "measures.h"
#include "messages.h"
#include "pins.h"

/*************************************************************************************
    Private macros
*************************************************************************************/

/* Sleep */
#define DEVICE_SLEEP    (180)

/*************************************************************************************
    Private variables
*************************************************************************************/
const Measures::types_t types[]  = {
    Measures::MEASURE_TEMPERATURE_CELSIUS,
    Measures::MEASURE_RELATIVE_HUMIDITY_PERCENT,
    Measures::MEASURE_LUMISOSITY_PERCENT,
    Measures::MEASURE_NONE,
    Measures::MEASURE_NONE,
};

//const uint8_t   sourceId[]        = {0xb6, 0xa9, 0x45, 0x58, 0x91};
//const uint8_t   sourceId[]        = {0xa5, 0xb3, 0x62, 0x12, 0x08};
const uint8_t   sourceId[]        = {0xb5, 0xe4, 0xf2, 0xff, 0x3e};

//const uint8_t   destinationId[]   = {0x9f, 0xa4, 0x28, 0x37, 0x5e};
const uint8_t   destinationId[]   = {0xef, 0xff, 0x3d, 0x4a, 0x97};

Messages messages(sourceId, destinationId);
Measures measures(const_cast<Measures::types_t*>(types));

/*************************************************************************************
    Private prototypes
*************************************************************************************/
void setup()
{
    /* GPIOS */
    digitalWrite(LED_EN, LOW);
    pinMode(LED_EN, OUTPUT);

    /* Interrupt */
    pinMode(INT1_PIN, INPUT);
    pinMode(INT2_PIN, INPUT);

    /* Inicializa mensagens */
    messages.begin(Messages::MESSAGE_MODE_NODE);

    /* Inicializa medidas */
    measures.begin();
}

void loop()
{
    /* Obtém as medidas */
    Measures::data_t data[MEASURE_SIZE];
    measures.read(data);
    
    /* Inicializa envio da mensagem */
    Messages::payload_t payload;
    digitalWrite(LED_EN, HIGH);
    messages.update(&payload, data);
    messages.send(&payload);
    digitalWrite(LED_EN, LOW);

    /* Entra em sleep */
    uint32_t timeout = 0;
    while (timeout < DEVICE_SLEEP)
    {
        LowPower.powerDown(SLEEP_8S, ADC_OFF, BOD_OFF);
        timeout += 8;
    }
}
